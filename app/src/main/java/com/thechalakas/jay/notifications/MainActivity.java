package com.thechalakas.jay.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity","onCreate");
        //get the buttons
        Button buttonnotification1 = (Button) findViewById(R.id.buttonnotification1);
        Button buttonnotification2 = (Button) findViewById(R.id.buttonnotification2);

        buttonnotification1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonnotification1 onClick");

                //build the notification
                Notification.Builder builder = new Notification.Builder(getApplicationContext());
                //set icon
                builder.setSmallIcon(R.mipmap.ic_launcher);
                //set title
                builder.setContentTitle("Notification 1");
                //set content
                builder.setContentText("Hello hello. Is anybody there. helloooo");

                //get the notification manager of the app
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                //make the notification happen
                manager.notify(1,builder.build());
            }
        });

        buttonnotification2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonnotification2 onClick");

                //build the notification
                //Notification.Builder builder = new Notification.Builder(getApplicationContext());
                android.support.v4.app.NotificationCompat.Builder builder= new NotificationCompat.Builder(getApplicationContext());
                //set icon
                builder.setSmallIcon(R.mipmap.ic_launcher);
                //set title
                builder.setContentTitle("Notification 1");
                //set an add action
                builder.addAction(R.mipmap.ic_launcher,"Add",null);
                //set a close action
                builder.addAction(R.mipmap.ic_launcher,"Close",null);
                //set content
                //builder.setContentText("Hello hello. Is anybody there. helloooo");
                builder.setStyle(new NotificationCompat.BigTextStyle().bigText("What is going on over here with this big and large notification"));

                //get the notification manager of the app
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                //make the notification happen
                manager.notify(1,builder.build());
            }
        });

    }
}
